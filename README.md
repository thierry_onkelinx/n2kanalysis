**Branch** | **Build status** | **Test coverage**
-- | -- | --
master | [![wercker status](https://app.wercker.com/status/38bcfec6bd0177b7b48297e66e49cd62/s/master "wercker status master ranch")](https://app.wercker.com/project/bykey/38bcfec6bd0177b7b48297e66e49cd62) | [![codecov.io](http://codecov.io/bitbucket/thierry_onkelinx/n2kanalysis/coverage.svg?branch=master)](http://codecov.io/bitbucket/thierry_onkelinx/n2kanalysis?branch=master)
develop | [![wercker status](https://app.wercker.com/status/38bcfec6bd0177b7b48297e66e49cd62/s/develop "wercker status develop branch")](https://app.wercker.com/project/bykey/38bcfec6bd0177b7b48297e66e49cd62) | [![codecov.io](http://codecov.io/bitbucket/thierry_onkelinx/n2kanalysis/coverage.svg?branch=develop)](http://codecov.io/bitbucket/thierry_onkelinx/n2kanalysis?branch=develop)

# The n2kanalysis package

The `n2kanalysis` package constains the main infrastructure for the analysis of the Natura 2000 Monitoring. The import from the raw data into the analysis object is done by dedicated packages, one for each monitoring scheme.
